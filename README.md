# Bateau mouche

### Bienvenue à la Fashion Week de Simplon.co !!!

L'objectif de ce projet est de recréer un site qui nous plaît. Nous avons choisi le site des **bateaux mouches Lyonnais** (https://www.lesbateauxlyonnais.com/), que nous allons recréer en *HTML* et *CSS* en utilisant la librairie *Bootstrap* afin d'organiser le contenu.

###### Contributeurs :

- Amandine Richerd ;
- Christina Chabert ;
- Sophie Brunier.



## Organisation du travail :

1. ***Maquetter le projet :***  

   - On refait sur papier le visuel du site ;

   - on décide de la structure *HTML* selon les différents éléments (classes *Bootstrap* ou style *CSS* ?!).

     

2. ***Page index.html :*** 

   - Recréer la structure de la page en utilisant des balises pertinentes ;
   - Utiliser des classes *Bootstrap* après avoir lier notre page HTML à la librairie du web.

   

3. ***Page style.css :*** 

   - Importer les photos et logos dans un dossier Images/ ;
   - Créer des @keyFrames pour les animations :
     - Zoom in-out de l'image d'accueil ;
     - Slide des 5 images ;
   - Créer @mediaQueries pour le responsive.

4. ***Refonte :***

   